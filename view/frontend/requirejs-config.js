/**
​ * ​ ​ Comment​ ​ for​ ​ file
​ *
​ * ​ ​ @category design
​ * ​ ​ @package RegionManager
​ * ​ ​ @author
 *   IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright​ ​ 2018​ ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */

var config = {
    map: {
        '*': {
            'Magento_Checkout/js/model/address-converter':'IAGC_RegionManager/js/model/address-converter',
            checkoutjs:'IAGC_RegionManager/js/checkout_js'
        }
    }
};
