<?php
/**
​ * ​ ​ IAGC
​ *
​ * ​ ​ PHP​ ​ Version​ ​ 7.0.22
​ *
​ * ​ ​ @category     IAGC
​ * ​ ​ @package    ​ ​ IAGC_RegionManager
​ * ​ ​ @author       IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright  ​ ​ 2018 ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license    ​ ​ http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */
/**
​ * ​ ​ Comment​ ​ for​ ​ file
​ *
​ * ​ ​ @category     IAGC
​ * ​ ​ @package    ​ ​ IAGC_RegionManager
​ * ​ ​ @author       IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright  ​ ​ 2018 ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license    ​ ​ http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */

namespace IAGC\RegionManager\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use IAGC\RegionManager\Api\Data\CitiesInterface;

class Cities extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('iagc_cities', CitiesInterface::ID);
    }
}
