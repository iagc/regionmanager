<?php
/**
​ * ​ ​ IAGC
​ *
​ * ​ ​ PHP​ ​ Version​ ​ 7.0.22
​ *
​ * ​ ​ @category     IAGC
​ * ​ ​ @package    ​ ​ IAGC_RegionManager
​ * ​ ​ @author       IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright  ​ ​ 2018 ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license    ​ ​ http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */
/**
​ * ​ ​ Comment​ ​ for​ ​ file
​ *
​ * ​ ​ @category     IAGC
​ * ​ ​ @package    ​ ​ IAGC_RegionManager
​ * ​ ​ @author       IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright  ​ ​ 2018 ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license    ​ ​ http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */


namespace IAGC\RegionManager\Block\Checkout;

use Magento\Framework\View\Element\Template;
use IAGC\RegionManager\Model\Config;


class Js extends \Magento\Framework\View\Element\Template
{
    protected $_config;

    public function __construct(
        Template\Context $context,
        Config $config,
        array $data = []
    )
    {
        $this->_config = $config;
        parent::__construct($context, $data);
    }

    public function enableModule()
    {
        return $this->_config->getEnableExtensionYesNo() == 1 ? true : false;
    }

    public function enableButtons()
    {
        return $this->_config->getEnableButtonsYesNo() == 1 ? true : false;
    }

}
