<?php
/**
​ * ​ ​ IAGC
​ *
​ * ​ ​ PHP​ ​ Version​ ​ 7.0.22
​ *
​ * ​ ​ @category     IAGC
​ * ​ ​ @package    ​ ​ IAGC_RegionManager
​ * ​ ​ @author       IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright  ​ ​ 2018 ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license    ​ ​ http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */
/**
​ * ​ ​ Comment​ ​ for​ ​ file
​ *
​ * ​ ​ @category     IAGC
​ * ​ ​ @package    ​ ​ IAGC_RegionManager
​ * ​ ​ @author       IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright  ​ ​ 2018 ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license    ​ ​ http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */

namespace IAGC\RegionManager\Block\Adminhtml\System\Zip;

class SelectZipCities extends \Magento\Framework\DataObject implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['label' => __('Please Select...') ,'value' => '']
        ];
    }
}
