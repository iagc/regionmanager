<?php
/**
​ * ​ ​ IAGC
​ *
​ * ​ ​ PHP​ ​ Version​ ​ 7.0.22
​ *
​ * ​ ​ @category     IAGC
​ * ​ ​ @package    ​ ​ IAGC_RegionManager
​ * ​ ​ @author       IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright  ​ ​ 2018 ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license    ​ ​ http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */
/**
​ * ​ ​ Comment​ ​ for​ ​ file
​ *
​ * ​ ​ @category     IAGC
​ * ​ ​ @package    ​ ​ IAGC_RegionManager
​ * ​ ​ @author       IAGC​ ​ Team​ ​ <info@iagc.com>
​ * ​ ​ @copyright  ​ ​ 2018 ​ IAGC​ ​ Ltd.​ ​ (https://www.iagc.com)
​ * ​ ​ @license    ​ ​ http://opensource.org/licenses/OSL-3.0​ ​ The​ ​ Open​ ​ Software​ ​ License​ ​ 3.0
​ */

namespace IAGC\RegionManager\Controller\Adminhtml\Cities;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('IAGC_RegionManager::cities');
        $resultPage->getConfig()->getTitle()->prepend(__('Cities'));

        return $resultPage;
    }

    /**
     * Is the user allowed to view the grid.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('IAGC_RegionManager::cities');
    }
}
